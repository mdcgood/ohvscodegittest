const appRoot = require('app-root-path'); 
const winston = require('winston');
const process = require('process');
 
const { combine, timestamp, label, printf } = winston.format;
 
const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});
 
const options = {
  file: {
    level: 'info',
    filename: `${appRoot}/logs/fcmpushEvent.log`,
    handleExceptions: true,
    json: false,
    maxsize: 5242880*2, // 10MB
    maxFiles: 10,
    colorize: false,
    format: combine(
      label({ label: 'Push event' }),
      timestamp(),
      myFormat
    )
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
    format: combine(
      label({ label: 'Push event Console' }),
      timestamp(),
      myFormat
    )
  }
}
 
let logger = new winston.createLogger({
  transports: [
    new winston.transports.File(options.file)
  ],
  exitOnError: false, 
});
 
if(process.env.NODE_ENV !== 'production'){
  logger.add(new winston.transports.Console(options.console)) // 개발 시 console로도 출력
}
 
module.exports = logger;
