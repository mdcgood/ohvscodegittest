
const express = require('express');
const cors = require('cors');
const fs = require('fs');
const path = require('path');
const logger = require('./config/winston');
//const HTTPS = require('https');


// Web push
const webpush = require('web-push');
// VAPID keys should only be generated only once.
const vapidKeys = webpush.generateVAPIDKeys();


// medical makers 서버키 
webpush.setGCMAPIKey('AAAAztQR8eE:APA91bFdS9CZDKukVWwoN2q3PoO0BhvjvrDyjA8wk2skFCslpRQtxD0IHKcIumFPAA3uFm0KdSwGw3crcp2ec3rPVeImGLwZPWd7CtcGDAzjjfFVKZibNECaYR4z9zW5A7WbheMVKJUq');
webpush.setVapidDetails(
	'mailto:mdcgood@dkitech.com',
	vapidKeys.publicKey,
	vapidKeys.privateKey
);


const app = express();

app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use(express.json());
// Routing

app.post('/push-send', function(req, res){
console.log(req.body.data);
    const subscription = req.body.subscription;
    const data = JSON.stringify(req.body.route);

	console.log("======>" + data);
	console.log("publicKey" + vapidKeys.publicKey);
	console.log("privateKey" + vapidKeys.privateKey);

    const options = {
      TTL: 24 * 60 * 60,
      vapidDetails: {
        subject: 'mailto:mdcgood@dkitech.com',
        publicKey: vapidKeys.publicKey,
        privateKey: vapidKeys.privateKey
      }
    };
  
  
    webpush.sendNotification( subscription, data, options );
    //return reply({statusCode:200, data : "OK"}).code(200);
	//return res.status(200).send({ "OK" });
	res.status(200).send("OK");
});


/* try {
  const option = {
	key: fs.readFileSync('/data/tools/apache2/conf/server.key'),
	cert: fs.readFileSync('/data/tools/apache2/conf/server.crt')
  };

  HTTPS.createServer(option, app).listen(3050, () => {
    console.log(`Connected....., 3050 port!`);
  });
} catch (error) {
  console.log('[HTTPS] HTTPS 오류가 발생하였습니다. HTTPS 서버는 실행되지 않습니다.');
  console.log(error);
} */


app.listen(3050, function(){
  logger.info('Connected, 3050 port!');
});